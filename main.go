package hello

import (
	"bufio"
	"os"
)

func CountLines(fileName string) (int, error) {
	file, err := os.Open(fileName)

	if err != nil {
		return 0, err
	}

	defer file.Close()

	fileScanner := bufio.NewScanner(file)
	fileScanner.Split(bufio.ScanLines)

	count := 0

	for fileScanner.Scan() {
		count++
	}

	return count, nil
}

func CountWords(fileName string) (int, error) {
	file, err := os.Open(fileName)

	if err != nil {
		return 0, err
	}

	defer file.Close()

	fileScanner := bufio.NewScanner(file)
	fileScanner.Split(bufio.ScanWords)

	count := 0

	for fileScanner.Scan() {
		count++
	}

	return count, nil
}
